import {Component, Input, OnInit} from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-module',
  templateUrl: './login-module.component.html',
  styleUrls: ['./login-module.component.css']
})
export class LoginModuleComponent implements OnInit {
    email: string;
    password: string;
    errorMsg: string;
    status: number;

  constructor( private router: Router) {}

  ngOnInit() {
  }

  login(email, password) {
      this.email = email;
      this.password = password;
      console.log('Email>>>' + this.email);
      console.log('password>>>' + this.password);
      if (this.email === 'username@gmail.com' && this.password === 'admin123') {
          this.status = 1;
          console.log('Login success');
          this.router.navigate(['/home']);
      } else {
          this.errorMsg = 'Username or password is wrong.';
      }
  }
}
