import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { LoginModuleComponent } from './login-module/login-module.component';
import {AppRoutingModule} from './app-routing.module';
import { HomeModuleComponent } from './home-module/home-module.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginModuleComponent,
    HomeModuleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
